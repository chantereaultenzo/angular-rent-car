import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { CarService } from '../car.service';
import { ICar } from '../car';

@Component({
  selector: 'app-cardetails',
  templateUrl: './cardetails.component.html',
  styleUrls: ['./cardetails.component.css']
})
export class CardetailsComponent implements OnInit {

  addCarForm: FormGroup;
  submitted: boolean = false;
  success: boolean = false;
  public cars = [];
  objForm: ICar;

  constructor(
      private _formBuilder: FormBuilder,
      private _carService: CarService) {
      this.addCarForm = this._formBuilder.group({
        id: ['', Validators.required],
        plateNumber: ['', Validators.required],
        brand: ['', Validators.required],
        price: ['', Validators.required]
      })
  }

  ngOnInit() {
  }

  onSubmit(){
      this.submitted = true;
      if (this.addCarForm.invalid) {
          return;
      }else{
          this.success = true;
          this.objForm = this.addCarForm.getRawValue();
          this._carService.addCar(this.objForm)
          .subscribe(data => this.cars.push(data));
      }
  }

}
