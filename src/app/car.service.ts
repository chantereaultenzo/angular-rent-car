import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable} from 'rxjs';

import { ICar } from './car';

@Injectable()
export class CarService {

  constructor(private http: HttpClient) {}

  getCars(): Observable<ICar[]>{
    return this.http.get<ICar[]>("http://localhost:8080/cars");
  }

  getCar(plateNumber): Observable<ICar[]>{
    return this.http.get<ICar[]>("http://localhost:8080/cars/" + plateNumber);
  }

  addCar(car: any): Observable<ICar[]>{
    return this.http.post<ICar[]>("http://localhost:8080/cars", car);
  }
}
