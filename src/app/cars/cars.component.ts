import { Component, OnInit } from '@angular/core';

import { CarService } from '../car.service';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  public cars = [];

  constructor(private _carService: CarService) { }

  ngOnInit() {
    this._carService.getCars()
    .subscribe(data => this.cars = data);
  }
}
