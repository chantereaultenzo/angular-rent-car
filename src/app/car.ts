export interface ICar {
  id: number;
  plateNumber: string;
  brand: string;
  price: number;
}
